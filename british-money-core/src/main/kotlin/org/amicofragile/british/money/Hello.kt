package org.amicofragile.british.money

class Hello {
    fun sayHello(to: String) = "Hello, ${to}!"
}