package org.amicofragile.british.money

class BritishMoney private constructor(private val totalPennies: Int) {
    constructor(pounds: Int, shillings: Int, pennies: Int) : this(pennies + shillings * PenniesPerShilling + pounds * PenniesPerPound)

    companion object {
        private const val PenniesPerShilling: Int = 12
        private const val ShillingsPerPound: Int = 20
        private const val PenniesPerPound: Int = ShillingsPerPound * PenniesPerShilling
    }

    val pennies: Int
        get() = totalPennies % PenniesPerShilling

    val shillings: Int
        get() = (totalPennies % PenniesPerPound) / PenniesPerShilling

    val pounds: Int
        get() = totalPennies / PenniesPerPound


    operator fun plus(that: BritishMoney): BritishMoney {
        return BritishMoney(this.totalPennies + that.totalPennies)
    }

    operator fun minus(that: BritishMoney): BritishMoney {
        return BritishMoney(this.totalPennies - that.totalPennies)
    }

    operator fun times(n: Int): BritishMoney {
        return BritishMoney(this.totalPennies * n)
    }

    operator fun div(i: Int): Pair<BritishMoney, BritishMoney> {
        var result = totalPennies / i
        var rest = totalPennies % i
        return Pair(BritishMoney(result), BritishMoney(rest))
    }

    override fun equals(other: Any?): Boolean {
        return other is BritishMoney && this.totalPennies == other.totalPennies
    }

    override fun hashCode(): Int {
        return this.totalPennies
    }
}