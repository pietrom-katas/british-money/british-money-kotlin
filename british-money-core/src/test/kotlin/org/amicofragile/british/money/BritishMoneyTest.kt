package org.amicofragile.british.money

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class BritishMoneyTest {
    @Test
    fun canInitializeNewInstance() {
        val money = BritishMoney(1, 1, 1)
        assertThat(money.pennies, `is`(equalTo(1)))
        assertThat(money.shillings, `is`(equalTo(1)))
        assertThat(money.pounds, `is`(equalTo(1)))
    }

    @Test
    fun canSumTwoInstances() {
        val m0 = BritishMoney(1, 1, 1)
        val m1 = BritishMoney(2, 2, 2)
        val sum = m0 + m1
        assertThat(sum.pennies, `is`(equalTo(3)))
        assertThat(sum.shillings, `is`(equalTo(3)))
        assertThat(sum.pounds, `is`(equalTo(3)))
    }

    @Test
    fun canSumTwoInstancesWithOverflow() {
        val m0 = BritishMoney(5, 11, 11)
        val m1 = BritishMoney(2, 11, 10)
        val sum = m0 + m1
        assertThat(sum.pennies, `is`(equalTo(9)))
        assertThat(sum.shillings, `is`(equalTo(3)))
        assertThat(sum.pounds, `is`(equalTo(8)))
    }

    @Test
    fun testPlusFromSpecification() {
        val result = BritishMoney(5, 17, 8) + BritishMoney(3,4,10)
        assertThat(result, `is`(equalTo((BritishMoney(9, 2, 6)))))
    }

    @Test
    fun testMinusFromSpecification() {
        val result = BritishMoney(5, 17, 8) - BritishMoney(3,4,10)
        assertThat(result, `is`(equalTo((BritishMoney(2, 12, 10)))))
    }

    @Test
    fun testMultiplyFromSpecification() {
        val result = BritishMoney(5, 17, 8) * 2
        assertThat(result, `is`(equalTo((BritishMoney(11, 15, 4)))))
    }

    @Test
    fun testDivideFromSpecification() {
        val (result, rest) = BritishMoney(18, 16, 1) / 15
        assertThat(result, `is`(equalTo((BritishMoney(1, 5, 0)))))
        assertThat(rest, `is`(equalTo((BritishMoney(0, 1, 1)))))
    }
}